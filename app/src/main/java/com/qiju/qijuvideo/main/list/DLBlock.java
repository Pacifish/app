package com.qiju.qijuvideo.main.list;

import com.qiju.qijuvideo.view.list.ItemList;

import java.util.List;

/**
 * Created by 幻陌
 * 首页列表块数据
 */

public class DLBlock {
    public int id;
    public String name;
    public String msg;
    public List<ItemList> data;
}
